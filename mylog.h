/*
 * Copyright (C) 2002 Mihai RUSU (dizzy@roedu.net)
 * Copyright (C) 2022  Chris Hutchindon <portmaster@BSDgorge.com>
 *
 * Rutine pentru logarea evenimentelor
 */

#ifndef __MYLOG_H__
#define __MYLOG_H__

void myopenlog();
void mylog(char *, ...);
void mycloselog();

#endif
