/*
  * Copyright (C) 2002  Mihai RUSU (dizzy@roedu.net)
  * Copyright (C) 2022  Chris Hutchinson <portmaster AT bsdforge.com>
  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#endif
#ifdef HAVE_SYS_PARAM_H
# include <sys/param.h>
#endif
#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif
#ifdef HAVE_FCNTL_H
# include <fcntl.h>
#endif
#ifdef HAVE_SYS_UN_H
# include <sys/un.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#endif
#ifdef HAVE_ERRNO_H
# include <errno.h>
#endif

static char *unixpath;
static int sock, term;
volatile static int quitasap = 0;

static void usage(void);
static int init_socket(char*);
static void close_socket();
static void mainloop(void);
static void read_sock(void);
static void read_term(void);

int main(int argc, char ** argv)
{
   if (argc != 2) usage();
   if (init_socket(argv[1]) < 0) return -1;
   mainloop();
   close_socket();
   return 0;
}

static void usage(void)
{
   printf("Usage: utelnet <unix-socket>\n");
   exit(-1);
}

static int init_socket(char *upath)
{
   struct sockaddr_un addr;
   
   if (upath == NULL || strlen(upath) >= MAXPATHLEN || strlen(upath) >= sizeof(addr.sun_path)) return -1;
   unixpath = upath;
   
   sock = socket(PF_UNIX, SOCK_STREAM, 0);
   if (sock < 0) {
      printf("Error on socket() (errno: %d str: '%s')\n", errno, strerror(errno));
      return -1;
   }

   addr.sun_family = AF_UNIX;
   strcpy(addr.sun_path, upath);

   if (connect(sock, (struct sockaddr *)&addr, sizeof(addr))) {
      printf("Error on connect() (errno: %d str: '%s')\n", errno, strerror(errno));
      close(sock);
      return -1;
   }
   
   term = open("/dev/tty", O_RDWR);
   if (term < 0) {
      printf("Error on /dev/tty (errno: %d str: '%s')\n", errno, strerror(errno));
      close(sock);
      return -1;
   }
   
   return 0;
}

static void close_socket(void)
{
   if (sock > 0) close(sock);
   if (term > 0) close(term);
}

static void mainloop(void)
{
   int maxsocket;
   int res;
   fd_set rfds;

   maxsocket = sock;
   if (maxsocket < term) maxsocket = term;
   while (!quitasap) {
      FD_ZERO(&rfds);
      FD_SET(sock, &rfds);
      FD_SET(term, &rfds);
      res = select(maxsocket + 1, &rfds, NULL, NULL, NULL);
      if (res <= 0) quitasap = 1;
      else {
	 if (FD_ISSET(sock, &rfds)) read_sock();
	 if (FD_ISSET(term, &rfds)) read_term();
      }
   }
}

static void read_sock(void)
{
   char buffer[1024];
   int res;
   
   if (sock < 1 || term < 1) {
      quitasap = 1;
      return;
   }
   
   res = read(sock , buffer, 1024);
   if (res <= 0) {
      quitasap = 1;
      return;
   }
   
   res = write(term, buffer, res);
   if (res <= 0) {
      quitasap = 1;
      return;
   }
}

static void read_term(void)
{
   char buffer[1024];
   int res;
   
   if (sock < 1 || term < 1) {
      quitasap = 1;
      return;
   }
   
   res = read(term, buffer, 1024);
   if (res <= 0) {
      quitasap = 1;
      return;
   }
   
   res = write(sock, buffer, res);
   if (res <= 0) {
      quitasap = 1;
      return;
   }
}
