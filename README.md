# UNIX Sockets 2 TCP forwarder


1. Description
    This is a small program to redirect all connections and traffic from a
UNIX socket to a (remote) TCP/IP address.

2. What it does
   tunnels all traffic between a (remote) address/port and a local UNIX socket
   does NOT use `fork()` (single process model)
   does use `O_NONBLOCK` , should be no case of blocking

3. What it does'nt do (yet):
   doesnt limit number of concurent connections

4. Installation
```
    ./configure
    make
    make install (this might require root privileges)
```
5. Examples

    5.1 Transparently forwarding of local MySQL connections to remote locations

    `unix2tcp /tmp/mysql.sock <remotesqlip> <remotesqlport>`
    Also you will need to have on remoteip:remoteport another portforwarder (this
    time a classic TCP to TCP one) to forward from `<remotesqlport> to 127.0.0.1:3306`
    so that the grant tables dont need to be changed. This example can work for any
    SQL server that uses UNIX sockets and doesnt change protocol in UNIX sockets
    than TCP ones.

    5.2 Forwarding of X connections

    Althought there are lots of features that X clients cannot do without local
    X server, there are some cases (as pointed out by a user) where you have
    binary distributed applications that want to talk only to local X server
    (even that they dont require any local-only X features). So using something like:

    `unix2tcp /tmp/.X11-unix/X0 <remoteXserver> 6000`

    can trick the local X apllications to think they talk to a local X server
    (you will also need a proper xhost command).

    5.3 Any other usefull examples please send me mail to include them here

6. Thanks to (in order of apparition :) ):
    * Vlad Galu
    * Szymon Roczniak
    * Philipp
    * David Hill
    * the FINK project (fink.sf.net)

7. Bugs, comments, feedback welcomed.

8. Source and support can be found at: https://codeberg.org/portmaster/unix2tcp/
