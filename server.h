/*
  * Copyright (C) 2003  Mihai RUSU (dizzy@roedu.net)
  * Copyright (C) 2022  Chris Hutchindon <portmaster@BSDgorge.com>
*/

#ifndef __SERVER_H__
#define __SERVER_H__

extern void server_process(void);

#endif /* __SERVER_H__ */
