#ifndef UNIX2TCP_VERSION
#define UNIX2TCP_VERSION "0.8.3"
#endif
#ifndef UNIX2TCP_NAME
#define UNIX2TCP_NAME "unix2tcp"
#endif
#ifndef UNIX2TCP_COPYRIGHT
#define UNIX2TCP_COPYRIGHT "Copyright (C) 2002-2004 Mihai RUSU, 2022 Chris Hutchinson"
#endif
