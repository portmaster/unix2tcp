/*
 * Copyright (C) 2002,2003 Mihai RUSU (dizzy@roedu.net)
 *  Copyright (C) 2022  Chris Hutchindon <portmaster@BSDgorge.com>
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#ifdef HAVE_SYSLOG_H
# include <syslog.h>
#endif
#ifdef HAVE_STDARG_H
# include <stdarg.h>
#endif
#include "version.h"
#include "mylog.h"

void myopenlog()
{
    openlog(UNIX2TCP_NAME, LOG_PID, 0);
}

void mylog(char * fmt, ...)
{
    va_list va;

    va_start(va, fmt);
    vsyslog(LOG_DAEMON | LOG_NOTICE, fmt, va);
    va_end(va);
}

void mycloselog()
{
    closelog();
}
