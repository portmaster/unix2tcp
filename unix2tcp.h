/*
  * Copyright (C) 2002,2003  Mihai RUSU (dizzy@roedu.net)
  * Copyright (C) 2022  Chris Hutchindon <portmaster@BSDgorge.com>
*/

#ifndef __UNIX2TCP_H__
#define __UNIX2TCP_H__

extern char *unixpath;
extern char *raddrs;
extern char *rports;
extern volatile int quitasap;

#endif /* __UNIX2TCP_H__ */
