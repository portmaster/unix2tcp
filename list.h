/*
 * Copyright (C) 2002  Mihai RUSU (dizzy@roedu.net)
 * Copyright (C) 2022  Chris Hutchindon <portmaster@BSDgorge.com>
*/

#ifndef __LINEAR_LIST_H__
#define __LINEAR_LIST_H__

#include <stdlib.h>

typedef struct list_elem_struct {
   void * data;
   struct list_elem_struct *prev, *next;
} t_list_elem;

typedef struct list_struct {
   t_list_elem *head, *tail;
   int no;
} t_list;

#define LIST_FIRST(list) ((list)->head)
#define LIST_LAST(list) ((list)->tail)
#define LIST_NEXT(p) ((p)->next)

extern struct list_struct * list_init(void);
extern int list_purge(t_list*);
extern int list_free(t_list*);
extern int list_insert_data(t_list*, void *);
extern int list_append_data(t_list*, void *);
extern int list_delete_by_elem(t_list*, t_list_elem *);
extern int list_get_size(t_list*);
extern void * list_fetch(t_list*);
extern t_list_elem* elem_create(void);
extern void elem_destroy(t_list_elem*);
extern int elem_set_data(t_list_elem *, void *);
extern void * elem_get_data(t_list_elem *);

#endif
