/*
  * Copyright (C) 2002,2003  Mihai RUSU (dizzy@roedu.net)
  * Copyright (C) 2022  Chris Hutchindon <portmaster@BSDgorge.com>
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#ifdef HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#endif
#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif
#ifdef HAVE_SYS_UN_H
# include <sys/un.h>
#endif
#ifdef HAVE_NETINET_IN_H
# include <netinet/in.h>
#endif
#ifdef HAVE_ARPA_INET_H
# include <arpa/inet.h>
#endif
#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif
#ifdef HAVE_FCNTL_H
# include <fcntl.h>
#endif
#ifdef HAVE_SIGNAL_H
# include <signal.h>
#endif
#ifdef HAVE_ERRNO_H
# include <errno.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#endif
#ifdef HAVE_SYS_PARAM_H
# include <sys/param.h>
#endif

#include "version.h"
#include "list.h"
#include "mylog.h"
#include "server.h"
#include "unix2tcp.h"

/* Global Variable Definitions */

char *unixpath;
char *raddrs;
char *rports;

static int daemonize = 0;

volatile int quitasap = 0;

static void dostop(int);

static int parse_args(int argc, char **argv);
static void usage(void);
static int do_daemonize(void);

int main(int argc, char **argv)
{

    if (parse_args(argc, argv)) return -1;

    if (do_daemonize()) return -1;
    signal(SIGINT, dostop);
    signal(SIGTERM, dostop);

    myopenlog();
    server_process();
    mycloselog();

    return 0;
}

static int parse_args(int argc, char ** argv)
{
    int idx;

    daemonize = 1;
    if (argc < 4) {
	usage();
	return -1;
    }

    idx = 1;
    if (argc == 5) {
	if (!strcmp(argv[idx], "-D")) {
	    daemonize = 0;
	    idx++;
	} else {
	    usage();
	    return -1;
	}
    }

    if (argv[idx]) unixpath = argv[idx];
    else {
	usage();
	return -1;
    }
    idx++;

    if (argv[idx]) raddrs = argv[idx];
    else {
	usage();
	return -1;
    }
    idx++;

    if (argv[idx]) rports = argv[idx];
    else {
	usage();
	return -1;
    }

    return 0;
}

static void usage(void)
{
    printf(UNIX2TCP_NAME" "UNIX2TCP_VERSION" "UNIX2TCP_COPYRIGHT"\n");
    printf("Usage: unix2tcp [-D] <unix-socket> <remote-ip> <remote-port>\n");
    printf("\t-D: dont try to run unix2tcp in background\n");
}

static int do_daemonize(void)
{
    if (!daemonize) return 0;

#ifdef HAVE_DAEMON
    if (daemon(0, 0)) return -1;
#endif

    return 0;
}

static void dostop(int n)
{
   quitasap = 1;
}
